FROM node:22.13.1-alpine3.21

ARG PORT=8080

ARG LOCALE="en_US.UTF-8"
ARG TZ="Europe/Prague"

ENV GID=1000 \
    LANG=${LOCALE} \
    LANGUAGE=${LOCALE} \
    LC_ALL=${LOCALE} \
    PORT=${PORT} \
    TERM=xterm \
    TZ=${TZ} \
    UID=1000 \
    GNAME=node \
    UNAME=node \
    WORKDIR=/var/www/app \
    npm_config_loglevel=error

COPY --chmod=755 usr/local/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

RUN ln -nfs /usr/share/zoneinfo/${TZ} /etc/localtime && \
    apk add -q --upgrade --no-cache --virtual .deps \
        bash \
        ca-certificates \
        dumb-init \
        g++ \
        gcc \
        git \
        make \
        pcre2 \
        python3 \
        py3-pip \
        py3-setuptools \
        zlib-dev \
        zlib \
    && \
    # Force certificate update
    update-ca-certificates && \
    # Python is needed for a number of js-related utilities to work
    ln -sf python3 /usr/bin/python && \
    npm install --location=global \
        npm@latest \
        node-gyp \
        webpack \
        # Non-interactive NPM login for automated build pipelines
        # See https://www.npmjs.com/package/npm-login-noninteractive
        npm-login-noninteractive \
    && \
    # Clean up
    rm -rf /var/cache/apk/* && \
    mkdir -p ${WORKDIR} && \
    chown ${UID}:${GID} ${WORKDIR}

WORKDIR ${WORKDIR}

USER ${UNAME}

EXPOSE ${PORT}

SHELL ["/bin/bash", "-c"]

ENTRYPOINT ["dumb-init", "--", "/usr/local/bin/docker-entrypoint.sh"]

CMD ["sleep", "infinity"]
